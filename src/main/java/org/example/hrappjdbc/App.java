package org.example.hrappjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.GregorianCalendar;
import org.example.model.Employee;
import com.mysql.cj.xdevapi.Result;

public class App {

	private static final String DB_URL = "jdbc:mysql://localhost:3306/hr ";
	private static final String USER = "root";
	private static final String PASSWORD = "";
	private static Connection connection;

	public static void main(String[] args) {

		try {
			connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
			String sql = " SELECT employee_id, first_name, last_name, email FROM employees";

			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int employee_id = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String email = resultSet.getString("email");

				System.out.print("EmployeeID: " + employee_id);
				System.out.print("FirstName: " + firstName);
				System.out.print("LastName: " + lastName);
				System.out.println("email: " + email);
			}

			sql = "SELECT employee_id, first_name, last_name, job_title from employees as e INNER JOIN jobs as j ON e.job_id=j.job_id";
			resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int employee_id = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String jobTitle = resultSet.getString("job_title");

				System.out.print("EmployeeID: " + employee_id);
				System.out.print("FirstName: " + firstName);
				System.out.print("LastName: " + lastName);
				System.out.println("Job Title: " + jobTitle);
			}

			sql = "SELECT e.employee_id, first_name, last_name, job_title, start_date from employees as e INNER JOIN jobs as j ON e.job_id=j.job_id inner join job_history as jh on e.employee_id=jh.employee_id";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				int employee_id = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String jobTitle = resultSet.getString("job_title");
				Date startDate = resultSet.getDate("start_date");

				System.out.print("EmployeeID: " + employee_id);
				System.out.print("FirstName: " + firstName);
				System.out.print("LastName: " + lastName);
				System.out.print("Job Title: " + jobTitle);
				System.out.println("Start date: " + startDate);
			}

			System.out.println("Update employee with id=204 to have job shipping clerk");
			sql = "update employees set job_id='SH_CLERK' where employee_id=204";
			statement.executeUpdate(sql);

			sql = "SELECT e.employee_id, first_name, last_name, job_title from employees as e INNER JOIN jobs as j ON e.job_id=j.job_id  where employee_id=204";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				int employee_id = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String jobTitle = resultSet.getString("job_title");

				System.out.print("EmployeeID: " + employee_id);
				System.out.print("FirstName: " + firstName);
				System.out.print("LastName: " + lastName);
				System.out.println("Job Title: " + jobTitle);

				System.out.println();
				System.out.println("SELECT THE DEPARTMENT THAT HAS THE HIGHEST NUMBER OF EMPLOYEES");
				sql = "select employees.department_id, department_name\r\n" + "from employees\r\n"
						+ "inner join departments\r\n" + "on employees.DEPARTMENT_ID=departments.DEPARTMENT_ID\r\n"
						+ "group by department_name\r\n" + "order by count(employee_id) desc\r\n" + "limit 1;";

				resultSet = statement.executeQuery(sql);
				while (resultSet.next()) {
					String departmentName = resultSet.getString("department_name");
					int departmentId = resultSet.getInt("department_id");

					System.out.print(" DepartmentName: " + departmentName);
					System.out.println(" DepartmentId: " + departmentId);
				}

				System.out.println();
				System.out.println("SELECT THE COUNTRY THAT HAS THE HIGHEST NUMBER OF DEPARTMENTS");
				sql = "select countries.country_id, countries.country_name\r\n" + "from countries\r\n"
						+ "inner join locations\r\n" + "on countries.COUNTRY_ID=locations.COUNTRY_ID\r\n"
						+ "inner join departments \r\n" + "on locations.LOCATION_ID=departments.LOCATION_ID\r\n"
						+ "group by country_name\r\n" + "order by count(department_id) desc\r\n" + "limit 1;";

				resultSet = statement.executeQuery(sql);
				while (resultSet.next()) {
					String countryName = resultSet.getString("country_name");
					String countryId = resultSet.getString("country_id");

					System.out.print(" CountryName: " + countryName);
					System.out.println(" CountryId: " + countryId);
				}
				System.out.println();
				System.out.println("SELECT THE AVERAGE SALARY FOR EACH DEPARTMENT");
				sql = "select departments.department_id, avg(salary) as AverageSalaryPerDepartment \r\n"
						+ "from employees \r\n" + "inner join departments \r\n"
						+ "on employees.department_id=departments.department_id \r\n" + "group by department_id desc;";

				resultSet = statement.executeQuery(sql);
				while (resultSet.next()) {
					int departmentId = resultSet.getInt("department_id");
					int averageSalary = resultSet.getInt("AverageSalaryPerDepartment");

					System.out.print(" DepartmentId: " + departmentId);
					System.out.println(" AverageSalary: " + averageSalary);
				}
			}

			// CALLING METHODS

			System.out.println();
			System.out.println("--------Using prepared statement to update and select an employee with job-------");
			updateEmployeeJob(204, "AD_PRES");
			getEmployeeById(204);

			System.out.println();
			System.out.println("------ Insert new employee into the database------");
			Employee employee = new Employee(207, "Ramona", "Cristea", "ramoCristea", "1234",
					new GregorianCalendar(2018, 7, 15).getTime(), "IT_PROG", (double) 100000, (double) 0.0, 204, 60);

			insertEmployee(employee);
			getEmployeeById(207);

			System.out.println("------opertaions in transaction starting here---------");
			updateEmployeeJob(203, "SH_CLERK");
			updateEmployeeAndInsertJobHistory("AC_MGR", 203, new java.util.Date(), new java.util.Date(), 0);
			getEmployeeById(203);

			System.out.println();
			System.out.println("--------employees who work in region Europe------------");
			getEmployeeWhoWorkInRegion("Europe\r");

			System.out.println();
			System.out.println(
					"------employees who work in IT Department with Salaries between x-y amount in the selected country------");
			getEmployeeInITwithSalaryFromCountry(1000, 6000, "United States of America");

			System.out.println();
			System.out.println("-----get maximum salary of an employee by job title----------- ");
			getMaximuSalaryByJobTitle("Programmer");

			System.out.println();
			System.out.println("----- all managers who work in a selected city----------- ");
			getManagerByCity("Toronto");

			System.out.println();
			System.out.println("----- list all jobs for selected employee----------- ");
			getAllJobsForEmployee("Whalen", "Jennifer");

			System.out.println();
			System.out.println("----- get minimum salary for selected employee----------- ");
			getMinumumSalaryForEmployee("Whalen", "Jennifer");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateEmployeeJob(int employeeId, String jobId) {
		String sql = "update employees set job_id=? where employee_id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, jobId);
			preparedStatement.setInt(2, employeeId);

			preparedStatement.executeUpdate();

			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void getEmployeeById(int employeeId) {
		String sql = "SELECT employee_id, first_name, last_name, job_title FROM employees AS e INNER JOIN jobs AS j ON e.job_id=j.job_id  WHERE e.employee_id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, employeeId);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeIdResult = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String jobTitle = resultSet.getString("job_title");

				System.out.print("EmployeeId: " + employeeIdResult);
				System.out.print("FirstName: " + firstName);
				System.out.print("LastName: " + lastName);
				System.out.print("jobTitle: " + jobTitle);
			}
			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void insertEmployee(Employee employee) {
		String sql = "INSERT INTO employees(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, employee.getEmployeeId());
			preparedStatement.setString(2, employee.getFirstName());
			preparedStatement.setString(3, employee.getLastName());
			preparedStatement.setString(4, employee.getEmail());
			preparedStatement.setString(5, employee.getPhoneNumber());
			preparedStatement.setDate(6, new java.sql.Date(employee.getHireDate().getTime()));
			preparedStatement.setString(7, employee.getJobId());
			preparedStatement.setDouble(8, employee.getSalary());
			preparedStatement.setDouble(9, employee.getCommisionPct());
			preparedStatement.setInt(10, employee.getManagerId());
			preparedStatement.setInt(11, employee.getDepartmentId());

			preparedStatement.executeUpdate();

			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateEmployeeAndInsertJobHistory(String jobId, int employeeId, Date startDate, Date endDate,
			int department) {
		String updateJobForEmployee = "UPDATE employees set job_id=? WHERE employee_id=?";
		String insertJobHistoryForEmployee = "INSERT into job_history(employee_id, start_date, end_date, job_id) VALUES(?,?,?,?)";

		try {
			connection.setAutoCommit(false);
			PreparedStatement updateJobForEmployeeStatement = connection.prepareStatement(updateJobForEmployee);
			updateJobForEmployeeStatement.setString(1, jobId);
			updateJobForEmployeeStatement.setInt(2, employeeId);

			updateJobForEmployeeStatement.executeUpdate();

			PreparedStatement insertJobHistoryStatement = connection.prepareStatement(insertJobHistoryForEmployee);
			insertJobHistoryStatement.setInt(1, employeeId);
			insertJobHistoryStatement.setDate(2, new java.sql.Date(startDate.getTime()));
			insertJobHistoryStatement.setDate(3, new java.sql.Date(endDate.getTime()));
			// insertJobHistoryStatement.setDate(3, null);
			insertJobHistoryStatement.setString(4, jobId);

			insertJobHistoryStatement.executeUpdate();

			connection.commit();// daca totul se executa cu succes facem commit-adica se exacuta in baza de data
			updateJobForEmployeeStatement.close();
			insertJobHistoryStatement.close();
			connection.setAutoCommit(false);

		} catch (SQLException e) {
			try {
				connection.rollback();// daca arunca exceptie facem roll back, adica invreseaza cele doua connections
										// de inainte
			} catch (SQLException e1) {
				try {
					connection.setAutoCommit(true);
				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	public static void getEmployeeWhoWorkInRegion(String regionName) {
		String sql = "select  e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, REGION_NAME from employees e inner join departments as d on e.DEPARTMENT_ID=d.DEPARTMENT_ID \r\n"
				+ "inner join locations on d.LOCATION_ID=locations.LOCATION_ID\r\n"
				+ "inner join  countries on locations.COUNTRY_ID=countries.COUNTRY_ID\r\n"
				+ "inner join regions on countries.REGION_ID= regions.REGION_ID where regions.REGION_name=? ";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, regionName);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String regionNameSql = resultSet.getString("region_name");

				System.out.print(" EmployeeId: " + employeeId);
				System.out.print(" FirstName: " + firstName);
				System.out.print(" LastName: " + lastName);
				System.out.println(" regionName: " + regionNameSql);
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void getEmployeeInITwithSalaryFromCountry(int minSalary, int maxSalary, String countryName) {
		String sql = "SELECT employee_id, first_name, last_name, department_name, country_name FROM employees INNER JOIN departments on employees.DEPARTMENT_ID=departments.DEPARTMENT_ID\r\n"
				+ "inner join locations on departments.LOCATION_ID=locations.LOCATION_ID\r\n"
				+ "inner join countries on locations.COUNTRY_ID=countries.COUNTRY_ID\r\n"
				+ "inner join regions on countries.REGION_ID=regions.REGION_ID\r\n"
				+ "WHERE DEPARTMENT_NAME='IT' AND SALARY >=? AND SALARY <=? AND  COUNTRY_NAME=?;\r\n";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, minSalary);
			preparedStatement.setInt(2, maxSalary);
			preparedStatement.setString(3, countryName);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String departmentName = resultSet.getString("department_name");
				String countryNameSql = resultSet.getString("country_name");

				System.out.print(" EmployeeId: " + employeeId);
				System.out.print(" FirstName: " + firstName);
				System.out.print(" LastName: " + lastName);
				System.out.print(" departmentName: " + departmentName);
				System.out.println(" countryName: " + countryNameSql);
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void getMaximuSalaryByJobTitle(String jobTitle) {
		String sql = "SELECT max(salary) as Maximum_Salary,EMPLOYEE_ID, FIRST_NAME, LAST_NAME FROM employees INNER JOIN jobs on employees.JOB_ID=jobs.JOB_ID\r\n"
				+ "WHERE JOB_TITLE=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, jobTitle);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int salary = resultSet.getInt("Maximum_Salary");
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");

				System.out.print(" MaximumSalary: " + salary);
				System.out.print(" EmployeeId: " + employeeId);
				System.out.print(" FirstName: " + firstName);
				System.out.println(" LastName: " + lastName);
			}
			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void getManagerByCity(String city) {

		String sql = "select first_name, last_name, employees.MANAGER_ID, CITY FROM employees inner join departments on employees.DEPARTMENT_ID=departments.DEPARTMENT_ID\r\n"
				+ "inner join locations on departments.LOCATION_ID=locations.LOCATION_ID\r\n"
				+ "WHERE  departments.MANAGER_ID is not null  and CITY=?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, city);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String idManager = resultSet.getString("manager_id");
				String cityName = resultSet.getString("city");

				System.out.print(" FirstName: " + firstName);
				System.out.print(" LastName: " + lastName);
				System.out.print(" ManagerId: " + idManager);
				System.out.println(" City " + cityName);
			}
			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void getAllJobsForEmployee(String lastName, String firstName) {

		String sql = "select employees.EMPLOYEE_ID, first_name, last_name, jobs.job_id, job_title, start_Date, end_Date from jobs inner join job_history on jobs.JOB_ID=job_history.JOB_ID\r\n"
				+ "inner join employees on job_history.employee_id=employees.EMPLOYEE_ID\r\n"
				+ "where last_Name=? and first_Name=?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, lastName);
			preparedStatement.setString(2, firstName);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("EMPLOYEE_ID");
				String firstNameSet = resultSet.getString("first_name");
				String lastNameSet = resultSet.getString("last_name");
				String idJob = resultSet.getString("job_id");
				String jobTitle = resultSet.getString("job_title");
				String startDate = resultSet.getString("start_date");
				String endDate = resultSet.getString("end_date");

				System.out.print(" EmployeeId: " + employeeId);
				System.out.print(" FirstName: " + firstNameSet);
				System.out.print(" LastName: " + lastNameSet);
				System.out.print(" JobId: " + idJob);
				System.out.print(" JobTitle " + jobTitle);
				System.out.print(" StartDate " + startDate);
				System.out.println(" EndDate" + endDate);
			}
			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void getMinumumSalaryForEmployee(String lastName, String firstName) {

		String sql = "select min(min_salary) as Minimum_Salary, employees.EMPLOYEE_ID, first_name, last_name, jobs.job_id, job_title, start_Date, end_Date from jobs inner join job_history on jobs.JOB_ID=job_history.JOB_ID\r\n"
				+ "inner join employees on job_history.employee_id=employees.EMPLOYEE_ID\r\n"
				+ "where last_Name=? and first_Name=?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, lastName);
			preparedStatement.setString(2, firstName);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int salary = resultSet.getInt("Minimum_Salary");
				int employeeId = resultSet.getInt("EMPLOYEE_ID");
				String firstNameSet = resultSet.getString("first_name");
				String lastNameSet = resultSet.getString("last_name");
				String idJob = resultSet.getString("job_id");
				String jobTitle = resultSet.getString("job_title");
				String startDate = resultSet.getString("start_date");
				String endDate = resultSet.getString("end_date");

				System.out.print(" Minimum_Salary: " + salary);
				System.out.print(" EmployeeId: " + employeeId);
				System.out.print(" FirstName: " + firstNameSet);
				System.out.print(" LastName: " + lastNameSet);
				System.out.print(" JobId: " + idJob);
				System.out.print(" JobTitle " + jobTitle);
				System.out.print(" StartDate " + startDate);
				System.out.println(" EndDate" + endDate);
			}
			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
